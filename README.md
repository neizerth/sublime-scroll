sublime-scroll v0.0.6 (alpha)
====================

"Sublime Text 2"-style scroll bars. Renders a visual scroll bar on right side of the webpage using css scaling.
Fork of: https://github.com/demux/sublime-scroll

## Installation

###Requires:

* jQuery v1.9.1 (http://jquery.com/)

## Versions

0.0.6

Fixed displaying large iframe

0.0.5

Fork: https://github.com/demux/sublime-scroll
